extends Node

var id =0;
var linked_miner=null;

var temp=-1;
var templogs=[];
var tempavg=-1;
var temp_ticks=1;
var temp_memory=-1;
var clock=-1;
var clock_memory=-1;
var watts=-1.0;
var fan_speed=-1;
var device_name="GPU-simulated";

#these are controlled by the miner tool.
var hashrate:int=0;

var display_bg:ColorRect = ColorRect.new();
var instanced_widget;

var tick:int=0;
var lastTick:Dictionary;

	
func set_miner(m):
	linked_miner=m;
	m.linked_devices.append(self);
	return self;
	
func set_pool(p):
	if(linked_miner!=null):
		linked_miner.pool=p;
	return self;

func set_id(index):
	id=index;
	return self;

func get_temp()->int: 
	return temp;
	
func get_temp_avg()->int: 
	return tempavg;

func set_temp(t): 
	temp=t;
	pass;

func get_temp_memory()->int: 
	return temp_memory;

func set_temp_memory(t): 
	temp_memory=t;
	pass;

func get_clock()->int: 
	return clock;

func set_clock(c): 
	clock=c;
	pass;

func get_clock_memory()->int: 
	return clock_memory;

func set_clock_memory(c): 
	clock_memory=c;
	pass;

func get_watts()->int: 
	return watts;

func set_watts(w): 
	watts=w;
	pass;

func get_fan()->int: 
	return fan_speed;

func set_fan(s): 
	fan_speed=s;
	pass;

func update_tick(host, container):
	if(lastTick==null):
		lastTick=OS.get_datetime(true);
		
	elif(lastTick.get("minute")!=OS.get_datetime(true).get("minute")):
		if(linked_miner!=null):
			linked_miner.update_tick(host, container);
		on_tick(host,container);
		templogs.append(temp);
		tempavg=0;
		if(templogs.size()>5):
			templogs.pop_at(0);
		for t in templogs:
			tempavg+=int(t);
		tempavg/=templogs.size();
		lastTick=OS.get_datetime(true);
	pass;

func on_tick(host, container):
	temp=HardwareMonitor.get_gpu_core_temp(id);
	temp_memory=HardwareMonitor.get_gpu_mem_temp(id);
	clock=HardwareMonitor.get_gpu_core_clock(id);
	clock_memory=HardwareMonitor.get_gpu_memory_clock(id);
	pass;

func is_cpu():
	return false;
