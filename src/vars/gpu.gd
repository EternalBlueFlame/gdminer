extends "res://src/vars/device.gd"

func on_tick(host, container):
	clock=HardwareMonitor.get_cpu_clock(id);
	temp=HardwareMonitor.get_cpu_temp(id);
	watts=HardwareMonitor.get_cpu_wattage(id);
	fan_speed=HardwareMonitor.get_cpu_fan(id);
	temp_memory=HardwareMonitor.get_ram_temp(id);
	clock_memory=HardwareMonitor.get_ram_clock(id);
	pass;

func is_cpu():
	return true;
