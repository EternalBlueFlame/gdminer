extends Node
class_name miningPool

var username="myOrg";
var password="x";
var worker="";

var coin_id=0;

var http:HTTPRequest=null;
var requesting=false;
var download_scene;
var miner_instance;

func getDisplayName():
	return "testpool";

#these are intended to be overridden 
func getAddress():
	return "-127.0.0.0:80";

func getUsername():
	return username;

func getPassword():
	return password;

func getWorker():
	return worker;
	
func getCrypto():
	return "";

func setCoinID(val:int):
	coin_id=val;
	return self;

func setup_nodes(host, device, node_above):
	return node_above;
	
func setup_advanced_nodes(host, device, node_above):
	return node_above;

func update_nodes(device):
	pass;


func pool_api_address()->String:
	return "";

func get_rest_method()->int:
	return HTTPClient.METHOD_GET;
func get_rest_payload()->String:
	return "";
func parse_api(logs:String):
	pass;

func call_rest_api(scene, miner):
	if(pool_api_address().length()<1):
		return;
	if(http==null):
		http = HTTPRequest.new();
		download_scene=scene;
		download_scene.add_child(http);
		http.connect("request_completed", self, "recieve_rest");
	var error = http.request(pool_api_address(),[],false,get_rest_method(),get_rest_payload());
	if(error ==OK):
		requesting=true;
		miner_instance=miner;
	return;

##called magically by parse_log
func recieve_rest(result, response_code, headers, body):
	if result == HTTPRequest.RESULT_SUCCESS:
		parse_api(body.get_string_from_utf8());
	requesting=false;
	pass;
