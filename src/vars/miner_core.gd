extends Node


var linked_devices = [];
var pool= null;
var processID =0;
var port:int=4000;
var display_bg:ColorRect = ColorRect.new();

var minerLog:=[[]];
var realtimeLog:String="";

var accepted:int = 0;
var rejected:int = 0;
var hashrateTotal:float =0.0;
var hashrateAvg:float =0.0;
var uptime:int=0;
var cooling_time:int=0;
#while these are pool values, they come from the mining tool itself. and may not be available.
var poolHashrate:int=0;
var poolBlock:int=0;
var poolEpoch:int=0;
var poolDifficulty:int=0;
var poolPing:int=0;
var pool_revenue:float=0;

var advanced_settings:bool=false;

#this is a scene and http references for the downloading process so unnecessary stuff can be removed after download
var download_scene;
var http:HTTPRequest=null;
var requesting=false;

func setup(host_display):
	for dev in linked_devices:
		dev.setup(host_display);
		pass;
	pass;

func combineWattage():
	var watts:int=0;
	for gpu in linked_devices :
		watts+=gpu.watts;
	
	if watts >0:
# warning-ignore:integer_division
		return str(watts)+"(" + str(watts/12)+"A)";#integer division, decimal is ignored, to change set 12 to float
	else:
		return "Unknown";


func get_run_string(pool:miningPool, port:int) -> String:
	return "";
	
func get_rest_address()->String:
	return "";

func get_rest_method()->int:
	return HTTPClient.METHOD_GET;

func get_rest_payload()->String:
	return "";

func update_tick(scene, widget):
	#update the internal log using data from the realtime log every 5 minutes.
	#todo: customizeable interval?
	if(is_running()):
		uptime+=1;
		call_rest_api(scene);
		pool.call_rest_api(scene, self);
	else:
		hashrateTotal=0;
		hashrateAvg=0;
	if(OS.get_datetime(true).get("minute")%5==0):
		print_debug("MINUTE 5");
	pass;

func call_rest_api(scene):
	if(http==null):
		http = HTTPRequest.new();
		download_scene=scene;
		download_scene.add_child(http);
		http.connect("request_completed", self, "recieve_rest");
	var error = http.request("http://127.0.0.1:"+str(port)+get_rest_address(),[]
		,false,get_rest_method(),get_rest_payload());
	if(error ==OK):
		requesting=true;
	return;

##called magically by parse_log
func recieve_rest(result, response_code, headers, body):
	if result == HTTPRequest.RESULT_SUCCESS:
		parse_logs(body.get_string_from_utf8());
	requesting=false;
	realtimeLog="";
	pass;

##each array entry in the minerLog is an entry from each 5 minutes.
##this is a double layer array, with the second layer being 
## the string output from the log with spaces removed and 
##  split into different strings by line
func parse_logs(logs:String):
	pass;

func check_version() -> String:
	return "";

func download_versions(scene):
	pass


func decompress_downloads(result, response_code, headers, body):
	if result != HTTPRequest.RESULT_SUCCESS:
		push_error("Could not download file.");
	else:
		if OS.get_name().to_lower()=="windows":
			Util.write_file(body, get_folder()+"Win64.zip");
		elif OS.get_name().to_lower()=="OSX":
			Util.write_file(body, get_folder()+"OSX.tar.gz");
		else:
			Util.write_file(body, get_folder()+"Linux_64.tar.gz");
			Util.extract(get_folder()+"Linux_64.tar.gz", get_folder());
	
	#this call can't really moved outside the method, so just copy-paste it from class to class
	download_scene.remove_child(http);
	pass

func init_download(scene):
	Util.download_7z(scene);
	if(http==null):
		http = HTTPRequest.new();
		download_scene=scene;
		download_scene.add_child(http);
		http.connect("request_completed", self, "decompress_downloads");
	download_versions(scene);
	if(pool!=null && pool.getUsername().length()>1):
		if(port==4000):
			scene.port_available+=1;
			port=scene.port_available;
		processID=Util.execute(get_run_string(pool, port));
	pass;
	
func get_file(address:String):
	if http.request(address) != OK:
		push_error("An error occurred in the HTTP request.");
	pass

func get_folder() -> String:
	return "";

func is_running() ->bool:
	return processID!=0;
