extends Node

const json_class=preload("res://src/common/json.gd");

const settings_field = preload("res://src/scenes/full/field.tscn");
const settings_button = preload("res://src/scenes/full/button.tscn");

static func add_vec(vec1, vec2):
	return Vector2(vec1.x+vec2.x,vec1.y+vec2.y);

static func vec_relative(vec1, canvas_item):
	return Vector2(vec1.x + canvas_item.anchor_left, 
	vec1.y + canvas_item.margin_top + canvas_item.get_size().y);

static func size_dynamic(x,y):
	return Vector2(OS.window_size.x*x,OS.window_size.y*y);
	
static func clear_widget(widget):
	for child in widget.get_children():
		widget.remove_child(child);
		pass;
	pass;

static func set_widget(host_display, device, scene):
	if (host_display==null):
		pass;
	clear_widget(device.display_bg);
	device.instanced_widget=scene.instance();
	device.display_bg.add_child(device.instanced_widget);
	host_display.add_child(device.display_bg);
	pass

static func instance_field(host_display, field, node_above, name, text, value):
	var node=field.instance();
	node.name=name;
	host_display.add_child(node);
	node.set_begin(vec_relative(Vector2(0,30),node_above));
	node.get_node("Label").text=text;
	if(typeof(value)==TYPE_STRING):
		node.get_node("TextEdit").text=value;
	elif(typeof(value)==TYPE_BOOL):
		node.get_node("Button").set_pressed_no_signal(value);
	return node;

static func hashrateReadable(hashrateTotal):
	if hashrateTotal>1000000000000000.0:
		return (str(hashrateTotal*0.000000000000001)+"PH/s");
	elif hashrateTotal>1000000000000.0:
		return (str(hashrateTotal*0.000000000001)+"TH/s");
	elif hashrateTotal>1000000000.0:
		return (str(hashrateTotal*0.000000001)+"GH/s");
	elif hashrateTotal>1000000.0:
		return (str(hashrateTotal*0.000001)+"MH/s");
	elif hashrateTotal>1000.0:
		return (str(hashrateTotal*0.001)+"KH/s");
	else:
		return (str(hashrateTotal)+"H/s");


static func write_file(data, path:String):
	var file = File.new();
	file.open(path, File.WRITE);
	file.store_buffer(data);
	file.close();
	pass
	

static func extract(path:String, output:String):
	var file = File.new();
	file.open(OS.get_user_data_dir() +"/extract.sh", File.WRITE);
	file.store_string("chmod +rx " +OS.get_user_data_dir()+"/binaries/7zz &&" + 
	OS.get_user_data_dir()+"/binaries/7zz e \""+path + "\" -o\"" +output + "\"");
	file.close();
	OS.execute("bash",[OS.get_user_data_dir()+"/extract.sh"],true,[],true, false);
	Directory.new().remove(OS.get_user_data_dir() +"/extract.sh");
	Directory.new().remove(path);
	if path.ends_with(".tar.gz"):
		extract(path.trim_suffix(".gz"),output);
	elif path.ends_with(".tar.xz"):
		extract(path.trim_suffix(".xz"),output);
	pass

static func mkdirs(path):
	var dict:Directory = Directory.new();
	dict.open("user://");
	dict.make_dir_recursive(path.trim_prefix("/"));
	
	return OS.get_user_data_dir() + path;

static func save_7z(result, response_code, headers, body):
	if result != HTTPRequest.RESULT_SUCCESS:
		push_error("Could not download file.");
	else:
		if(OS.get_name().to_lower()=="windows"):
			pass;
		elif(OS.get_name().to_lower()=="OSX"):
			pass;
		else:
			mkdirs("/binaries/");
			write_file(body, OS.get_user_data_dir()+"/binaries/7zz");
	pass;
	
static func exists_7z():
	if(OS.get_name().to_lower()=="windows"):
		return false;
	elif(OS.get_name().to_lower()=="OSX"):
		return false;
	else:
		return File.new().file_exists(OS.get_user_data_dir()+"/binaries/7zz");
	pass;
	
static func download_7z(scene):
	if(exists_7z()):
		return;
	var http = HTTPRequest.new();
	scene.add_child(http);
	http.connect("request_completed", load("res://src/common/util.gd"), "save_7z");
	if http.request("https://gitlab.com/EternalBlueFlame/gdminer/-/raw/main/bin/7zz?inline=false") != OK:
		push_error("An error downloading 7z.");
		scene.remove_child(http);
	pass;

static func execute(command:String)->int:
	if(OS.get_name().to_lower()=="windows"):
		return OS.execute("cmd.exe",[command], false,[],true,false);
	elif(OS.get_name().to_lower()=="OSX"):
		return OS.execute("bash",["-c",command], false,[],true,false);
	else:
		print_debug(command);
		return OS.execute("bash",["-cvxs \""+command+"\""], false,[],true,false);
		


static func json_load(json:String):
	return json_class.new().create(json);
