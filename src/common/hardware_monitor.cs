using Godot;
using System;
using System.Collections.Generic;
using LibreHardwareMonitor.Hardware;
using ATI.ADL;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using Nvidia.Nvml;

public class hardware_monitor : Node {
	
	private ATI.ADL.ADLAdapterInfoArray gpuAMD;
	private int ADL_total_devices=0; //the ADL library doesn't seem to have an option for tracking the non-null count, do it manually
	private List<IntPtr> gpuNvidia = new List<IntPtr>();
	//private Cloo.ComputeContext gpuIntel=null;
	//private List<OpenHardwareMonitor.Hardware.Nvidia.> gpuNvidia=new List<nvmlDevice>();
	
	private List<ISensor> cpuTempSensor = new List<ISensor>();
	private List<List<ISensor>> cpuClockSensor = new List<List<ISensor>>();
	private List<ISensor> cpuFanSensor = new List<ISensor>();
	private List<ISensor> cpuWattSensor = new List<ISensor>();
	
	private List<ISensor> ramTempSensor = new List<ISensor>();
	private List<ISensor> ramClockSensor = new List<ISensor>();
	private List<ISensor> ramWattSensor = new List<ISensor>();
	
	
	private List<LibreHardwareMonitor.Hardware.Cpu.GenericCpu> cpuIntel=
		new List<LibreHardwareMonitor.Hardware.Cpu.GenericCpu>();
	private List<LibreHardwareMonitor.Hardware.Cpu.GenericCpu> cpuAMD=
		new List<LibreHardwareMonitor.Hardware.Cpu.GenericCpu>();
	private List<LibreHardwareMonitor.Hardware.Cpu.GenericCpu> cpuGeneric=
		new List<LibreHardwareMonitor.Hardware.Cpu.GenericCpu>();
		
	static Process sensors = new Process();
	public string debugLog="";
	
	private Computer computer;
	//runs on prject start becaue class is autolloaded
	public override void _Ready() {
		debugLog="";
		
		debugLog+="\nLibreHWM initializing\n";
		computer = new Computer();
		computer.IsCpuEnabled = true;
		computer.IsMotherboardEnabled = true;
		computer.IsMemoryEnabled = true;
		computer.IsGpuEnabled = false;
		computer.IsControllerEnabled = true;
		computer.IsStorageEnabled = true;
		computer.Open();
		int indexCpu=0;
		int indexMemory=0;
		foreach (IHardware hardwareItem in computer.Hardware){
			hardwareItem.Update();
			if (hardwareItem.HardwareType == HardwareType.Cpu){
				if(hardwareItem.Name.ToLower().Contains("intel")){
					cpuIntel.Add((LibreHardwareMonitor.Hardware.Cpu.GenericCpu)hardwareItem);
				} else if(hardwareItem.Name.ToLower().Contains("amd") ||
					hardwareItem.Name.ToLower().Trim().Contains("advancedmicrodevice")){
					cpuAMD.Add((LibreHardwareMonitor.Hardware.Cpu.GenericCpu)hardwareItem);
				} else {//todo: we should really check if it's _actually_ ARM or some chineese witchcraft
					cpuGeneric.Add((LibreHardwareMonitor.Hardware.Cpu.GenericCpu)hardwareItem);
				}
				
				//init the sensor caches.
				foreach (var s in hardwareItem.Sensors){
					if(s!=null){
						if (s.SensorType == SensorType.Temperature && s.Name.ToLower().Contains("package")){
							if(s.Value.HasValue && s.Value!=0){
								cpuTempSensor.Add(s);
							}
						} else if (s.SensorType == SensorType.Clock){
							if(cpuClockSensor.Count==indexCpu){
								cpuClockSensor.Add(new List<ISensor>());
							}
							if (s.Value.HasValue && s.Value!=0){
								cpuClockSensor[indexCpu].Add(s);
							}
						} else if (s.SensorType == SensorType.Fan){
							if(s.Value.HasValue && s.Value!=0){
								cpuFanSensor.Add(s);
							}
						} else if (s.SensorType == SensorType.Power){
							if(s.Value.HasValue && s.Value!=0){
								cpuWattSensor.Add(s);
							}
						}
					}
				}
				//make sure there's always an index for the sensor, even if there wasn't a sensor
				if(cpuTempSensor.Count<=indexCpu){
					cpuTempSensor.Add(null);
				}
				if(cpuClockSensor.Count<=indexCpu){
					cpuClockSensor.Add(null);
				}
				if(cpuFanSensor.Count<=indexCpu){
					cpuFanSensor.Add(null);
				}
				if(cpuWattSensor.Count<=indexCpu){
					cpuWattSensor.Add(null);
				}
				indexCpu++;
			} else if (hardwareItem.HardwareType == HardwareType.Memory){
				foreach (var s in hardwareItem.Sensors){
					if(s!=null){
						if (s.SensorType == SensorType.Temperature){
							if(s.Value.HasValue && s.Value!=0){
								ramTempSensor.Add(s);
							}
						} else if (s.SensorType == SensorType.Clock){
							if(s.Value.HasValue && s.Value!=0){
								ramClockSensor.Add(s);
							}
						} else if (s.SensorType == SensorType.Power){
							if(s.Value.HasValue && s.Value!=0){
								ramWattSensor.Add(s);
							}
						}
					}
				}
				//make sure there's always an index for the sensor, even if there wasn't a sensor
				if(ramTempSensor.Count<=indexMemory){
					ramTempSensor.Add(null);
				}
				if(ramClockSensor.Count<=indexMemory){
					ramClockSensor.Add(null);
				}
				if(ramWattSensor.Count<=indexMemory){
					ramWattSensor.Add(null);
				}
				indexMemory++;
			}
		}
		computer.Close();
		
		GD.Print("LibreHWM Initialized");
		debugLog+="\nLibreHWM Initialized with " + get_cpu_count() +" Devices";
		
		
		debugLog+="\nInitilizing NVML\n";
		try{
			NvGpu.NvmlInitV2();
			uint i=0;
			var device = NvGpu.NvmlDeviceGetHandleByIndex(i);
			while (device!=null){
				debugLog+="\n-----------Nvidia NVML Found Device---------------";
				debugLog+="\nAdapter #{"+i+"} " + device.ToString();
				debugLog+="\nDevice Name: #{"+i+"} " + NvGpu.NvmlDeviceGetName(device,120);
				debugLog+="\n--------------------------------------------------";
				GD.Print("Adding GPU");
				gpuNvidia.Add(device);
				i++;
				device=NvGpu.NvmlDeviceGetHandleByIndex(i);

			}
		} catch (Exception e){
			//this makes it so it won't print an error 
			if(!e.ToString().Contains("NVML_ERROR_INVALID_ARGUMENT")){
				debugLog+=e.ToString();
			}
		}
		/*int gpuCount = NVApi.GetPhysicalGPUCount();
		for (int i = 0; i < gpuCount; i++)
		{
			string gpuName = NVApi.GetPhysicalGPUName(i);
			debugLog+=gpuName;
		}*/
		
		
		debugLog+="\nInitializing ADL";
		gpuAMD = new ADLAdapterInfoArray{
			ADLAdapterInfo = new ADLAdapterInfo[ADL.ADL_MAX_ADAPTERS]
		};
		
		
		
		int adapterStatus=0;
		int adapters =0;
		// Get the adapter information
		for (int i = 0; i < gpuAMD.ADLAdapterInfo.Length; i++){
			var adapterInfo = gpuAMD.ADLAdapterInfo[i];

			// Check if the adapter is active
			if (ADL.ADL_Adapter_Active_Get !=null &&
					ADL.ADL_Adapter_Active_Get(gpuAMD.ADLAdapterInfo[i].AdapterIndex, ref adapterStatus) != 0) {
				debugLog+="\n-------------AMD ADL Found Device-----------------";
				debugLog+="\nAdapter #{"+i+"}" + adapterInfo.AdapterIndex;
				debugLog+="\nVendor ID: {"+i+"}" + adapterInfo.VendorID;
				debugLog+="\nBus Number: {"+i+"}" + adapterInfo.BusNumber;
				debugLog+="\nDevice Name: {"+i+"}" + adapterInfo.AdapterName;
				debugLog+="\n--------------------------------------------------";
				adapters++;
				ADL_total_devices++;
			}
		}
		debugLog+="\nAMD ADL Initialized with " + adapters + " Devices.";
		GD.Print("AMD ADL Initialized");
		
		if(System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Linux)){
			sensors.StartInfo.RedirectStandardOutput = true;
			sensors.StartInfo.UseShellExecute = false;
			sensors.StartInfo.CreateNoWindow = true;
			
			sensors.StartInfo.FileName = "/bin/bash";
			sensors.StartInfo.Arguments="-c sensors";
			sensors.Start();
			StreamReader reader = sensors.StandardOutput;
			string output = reader.ReadToEnd();
			if(output.Length > 100){
				debugLog+="\nlm-sensors found";
			} else {
				debugLog+="\npackage lm-sensors not installed! - Some hardware may not be able to be monitored.";
				sensors = null;
			}
		} else {
			sensors=null;
		}
		
		
		debugLog+="\nMonitor Ready";
		GD.Print("Monitor Ready");
	}


/**
*
* API REFERENCES
*
*/

/**
* Central Processing Unit
*
* DOCS: https://github.com/LibreHardwareMonitor/LibreHardwareMonitor/tree/master/LibreHardwareMonitorLib
*
*NOTES:
	the CPU Id system goes in order of: intel, AMD, ARM
*/

	//This method can be mostly ignored, it's used to get the CPU device instance by ID.
	private LibreHardwareMonitor.Hardware.Cpu.GenericCpu get_cpu(int id){
		if(id>cpuIntel.Count+cpuAMD.Count){
			return cpuGeneric[id-cpuIntel.Count-cpuAMD.Count];
		} else if(id>cpuIntel.Count){
			return cpuAMD[id-cpuIntel.Count];
		}
		return cpuIntel[id];
	}

	//Returns 
	public int get_cpu_count(){
		return cpuIntel.Count+cpuAMD.Count + cpuGeneric.Count;
	}

	public string get_cpu_name(int id){
		return get_cpu(id).Name;
	}

	public int get_cpu_clock(int id){
		if(cpuClockSensor[id].Count>0){
			int clockAverage=0;
			foreach(ISensor s in cpuClockSensor[id]){
				clockAverage+=(int)s.Value;
			}
			return clockAverage/cpuClockSensor[id].Count;
		}
		return 0;
	}

	public int get_cpu_temp(int id){
		if(cpuTempSensor[id]!=null){
			return (int)cpuTempSensor[id].Value;
		}
		if(sensors ==null){
			return 0;
		}
		string output="";
		// Start the process and read the output
		if(System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Linux)){
			sensors.Start();
			StreamReader reader = sensors.StandardOutput;
			output = reader.ReadToEnd();
			output=output.Substring(output.IndexOf("Package id "+ id.ToString())+12);
			output=output.Substring(output.IndexOf("+")+1);
			output = output.Substring(0, output.IndexOf("."));
		}
		if(output.Length()>0){
			return Convert.ToInt32(output);
		} else {
			return 0;
		}
	}

	public int get_cpu_wattage(int id){
		if(cpuWattSensor[id]!=null){
			return (int)cpuWattSensor[id].Value;
		}
		return 0;
	}
	
	public int get_cpu_fan(int id){
		if(cpuFanSensor[id]!=null){
			return (int)cpuFanSensor[id].Value;
		}
		string output="";
		// Start the process and read the output
		if(System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Linux)){
			sensors.Start();
			StreamReader reader = sensors.StandardOutput;
			output = reader.ReadToEnd();
			output=output.Substring(output.IndexOf("fan")+5);
			output = output.Substring(0, output.IndexOf("RPM"));
			output=output.Trim();
		}
		if(output.Length()>0 && output.Length()<6){
			return Convert.ToInt32(output);
		} else {
			return 0;
		}
	}
	public int get_ram_temp(int id){
		if(ramTempSensor[id]!=null){
			return (int)ramTempSensor[id].Value;
		}
		return 0;
	}
	
	public int get_ram_clock(int id){
		if(ramClockSensor[id]!=null){
			return (int)ramClockSensor[id].Value;
		}
		return 0;
	}

	public int get_ram_free(){
		return 0;
	}

	public int get_ram_wattage(int id){
		if(ramWattSensor[id]!=null){
			return (int)ramWattSensor[id].Value;
		}
		return 0;
	}

/**
* Graphics Processing Unit
*
* DOCS: https://github.com/clSharp/Cloo/tree/master/Cloo/Source
*
*NOTES:
	the GPU Id system goes in order of: Nvidia, AMD, Intel
*/
	private void get_gpu(int id){
		//This feels like an absolutley horrible way to do this, 
		//  but considering these can be null, I don't see much better options
		//  Maybe combine arrays in the future? they may not need to be seperated like this...
		/*int count=0;
		if(gpuNvidia!=null){
			count=gpuNvidia.Devices.Count;
		}
		if(gpuAMD!=null){
			count+=gpuAMD.Devices.Count;
		}
		if(id>count){
			return gpuIntel.Devices[id-gpuNvidia.Devices.Count-gpuAMD.Devices.Count];
		}
		if(gpuNvidia!=null){
			count=gpuNvidia.Devices.Count;
		}
		if(id>count){
			return gpuAMD.Devices[id-gpuNvidia.Devices.Count];
		}
		return gpuNvidia.Devices[id];*/
	}
	
	public int get_gpu_count(){
		int val=0;
		if(gpuNvidia!=null){
			val+=gpuNvidia.Count;
		}
		if(gpuAMD.ADLAdapterInfo!=null){
			val+=ADL_total_devices;
		}
//		if(gpuIntel!=null){
//			val+=gpuIntel.Devices.Count;
//		}
		return val;
	}
	
	public bool is_gpu_amd(int id){
		return id>gpuNvidia.Count;
	}

	public string get_gpu_name(int id){
		if(is_gpu_amd(id)){
			return gpuAMD.ADLAdapterInfo[id-gpuNvidia.Count].AdapterName;
		} else if(gpuNvidia.Count>id) {
			return NvGpu.NvmlDeviceGetName(gpuNvidia[id],120);
		}
		return "";//get_gpu(id).Name;
	}

	public int get_gpu_core_temp(int id){
		if(is_gpu_amd(id)){
			ADLTemperature temp = new ADLTemperature();
			int t=200;
			for(int i=0;i<8;i++){
				ADL.ADL_Overdrive5_Temperature_Get(id-gpuNvidia.Count,i,ref temp);
				if(temp.Temperature>0 && temp.Temperature<t){
					t=temp.Temperature;
				}
			}
			return t;
		} else if(gpuNvidia.Count>id) {
			return (int)NvGpu.NvmlDeviceGetTemperature(gpuNvidia[id], NvmlTemperatureSensor.NVML_TEMPERATURE_GPU);
		}
		return 0;
	}
	
		public int get_gpu_mem_temp(int id){
		if(is_gpu_amd(id)){
			ADLTemperature temp = new ADLTemperature();
			int t=0;
			for(int i=0;i<8;i++){
				ADL.ADL_Overdrive5_Temperature_Get(id-gpuNvidia.Count,i,ref temp);
				if(temp.Temperature>t){
					t=temp.Temperature;
				}
			}
			return t;
		} else if(gpuNvidia.Count>id) {
			return (int)NvGpu.NvmlDeviceGetTemperature(gpuNvidia[id], NvmlTemperatureSensor.NVML_TEMPERATURE_MEM);
		}
		return 0;
	}
	
	public int get_gpu_core_clock(int id){
		if(is_gpu_amd(id)){
			ADLODNPerformanceStatus stats = new ADLODNPerformanceStatus();
			stats=ADL.ADL2_OverdriveN_PerformanceStatus_Get(new IntPtr(0),id-gpuNvidia.Count);
			return stats.CoreClock;
		} else if(gpuNvidia.Count>id) {
			return (int)NvGpu.nvmlDeviceGetClockInfo(gpuNvidia[id], NvmlClockType.NVML_CLOCK_GRAPHICS);
		}
		return 0;
	}
	
		public int get_gpu_memory_clock(int id){
		if(is_gpu_amd(id)){
			ADLODNPerformanceStatus stats = new ADLODNPerformanceStatus();
			stats=ADL.ADL2_OverdriveN_PerformanceStatus_Get(new IntPtr(0),id-gpuNvidia.Count);
			return stats.MemoryClock;
		} else if(gpuNvidia.Count>id) {
			return (int)NvGpu.nvmlDeviceGetClockInfo(gpuNvidia[id], NvmlClockType.NVML_CLOCK_MEM);
		}
		return 0;
	}
}
