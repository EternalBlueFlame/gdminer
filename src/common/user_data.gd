extends Node

#cached node value, faster than having to initialize the variable every interaction.
# used for main_display.gd@display_value
var current_node:Node = null;

