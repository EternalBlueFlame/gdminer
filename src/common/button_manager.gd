extends Control


var Main;
var following = false;
var dragging_start_position:Vector2= Vector2(0,0);

var button_close; var button_min; var button_max;
var button_dashboard; var button_settings; var button_logs; var button_about;
var display_scrollbar;


func _ready():
	button_close=Main.get_node("banner_bg/topbar/close_button");
	button_close.connect("pressed", self, "button_close");
	
	button_max=Main.get_node("banner_bg/topbar/MaximizeButton");
	button_max.connect("pressed", self, "button_max");
	
	button_min=Main.get_node("banner_bg/topbar/MinimizeButton");
	button_min.connect("pressed", self, "button_min");
	
	
	button_close=Main.get_node("banner_bg/topbar");
	button_close.connect("gui_input", self, "window_drag");
	
	
	button_dashboard=Main.get_node("sidebar/start_button");
	button_dashboard.connect("pressed", self, "button_dashboard");
	
	button_settings=Main.get_node("sidebar/settings_button");
	button_settings.connect("pressed", self, "button_settings");
	
	button_logs=Main.get_node("sidebar/logs_button");
	button_logs.connect("pressed", self, "button_logs");
	
	button_about=Main.get_node("sidebar/about_button");
	button_about.connect("pressed", self, "button_about");
	
	display_scrollbar=Main.get_node("miner_scroll_bar");
	
	pass;

func window_drag(event):
	if (event is InputEventMouseButton):
		if (event.get_button_index() == 1 && event.pressed):
			following = true;
			print_debug("input")
			dragging_start_position=Main.get_local_mouse_position();
		elif (event.get_button_index() == 1):
			following=false;
	pass;

func _process(_delta):
	if (following):
		var pos:Vector2 = Main.get_global_mouse_position()-dragging_start_position;
		if (abs(pos.x)>=1 || abs(pos.y)>=1):
			OS.set_window_position(OS.window_position+pos);
	pass;


func scrollbar(event):
	
	if (event is InputEventKey):
		if (event.scancode == KEY_UP):
			if (display_scrollbar.value>0):
				display_scrollbar.value-=30;
		elif (event.scancode==KEY_DOWN):
			if (display_scrollbar.value<Main.scrollmax):
				display_scrollbar.value+=30;
		elif (event.scancode==KEY_PAGEDOWN):
			display_scrollbar.value+=min(Main.scrollmax-(100), 100);
		elif (event.scancode==KEY_PAGEUP):
			display_scrollbar.value-=min(Main.scrollmax-(100), 100);
	if (event is InputEventMouseButton):
		if (event.button_index == BUTTON_WHEEL_UP):
				display_scrollbar.value-=20;
		elif (event.button_index == BUTTON_WHEEL_DOWN):
				display_scrollbar.value+=20;
			
	pass;

func button_close():
	for m in Main.devices:
		if m.linked_miner!=null && m.linked_miner.processID!=0:
			OS.kill(m.linked_miner.processID);
	Main.get_tree().quit();
	pass;

func button_min():
	OS.window_minimized=true;
	pass;

func button_max():
	OS.window_maximized=!OS.window_maximized;
	pass;

func button_dashboard():
	if(Main.menu!=0):
		Main.menu=0;
		Main.init_screen();
	elif (!Main.running):
		Main.running=true;
		Main.init_screen();
		for m in Main.devices:
			if m.linked_miner!=null:
				m.linked_miner.init_download(Main);
	else:
		Main.running=false;
		Main.init_screen();
		for m in Main.devices:
			if (m.linked_miner!=null && m.linked_miner.processID!=0):
				OS.kill(m.linked_miner.processID);
				m.linked_miner.processID=0;
		

	pass

func button_settings():
	Main.menu=1;
	Main.init_screen();
	pass

func button_logs():
	Main.menu=4;
	Main.init_screen();

	pass

func button_about():
	Main.menu=8;
	Main.init_screen();

	pass





func init_sidebar():
	if(Main.menu!=0):
		button_dashboard.set_text("Dashboard");
	elif(Main.running):
		button_dashboard.set_text("Stop");
	else:
		button_dashboard.set_text("Start");
	display_scrollbar.value=0;
	
	if(Main.menu!=1):
		button_settings.show();
	else:
		button_settings.hide();
		
	if(Main.menu!=4):
		button_logs.show();
	else:
		button_logs.hide();
	
	if(Main.menu!=8):
		button_about.show();
	else:
		button_about.hide();
	pass
