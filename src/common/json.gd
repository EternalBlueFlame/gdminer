extends Node

var string:String;

func create(json):
	string=json;
	return self;


func get(value:String):
	string=string.get_slice(value,1);
	return self;

func parse_object() -> String:
	return string.get_slice(": ",1).get_slice(",",0);

func parse_int() -> int:
	return int(string.get_slice(": ",1).get_slice(",",0));

func parse_float() -> float:
	return float(string.get_slice(": ",1).get_slice(",",0));

func parse_array() -> PoolStringArray:
	return string.get_slice("[",1).get_slice("]",0).split(",");


func _ready():
	pass

