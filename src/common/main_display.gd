extends Node

const device_scene = preload("res://temp_device_widget.tscn");
const settings_scene = preload("res://src/scenes/full/settings_widget.tscn");

func display_unattached_device():
	pass;
	
	
static func init_settings(node_above, device):
	if(node_above!=null):
		Util.set_widget(node_above, device, settings_scene);
	
	if(device.linked_miner!=null && device.display_bg.has_node("widget_bg/Button_miner")):
		device.display_bg.get_node("widget_bg/Button_miner").text = device.linked_miner.get_name();
		device.display_bg.get_node("widget_bg/Button_pool").text = device.linked_miner.pool.getDisplayName();
		if(device.linked_miner.pool!=null):
			var last_node=device.display_bg.get_node("widget_bg/Button_pool");
			var bg=device.display_bg.get_node("widget_bg");
			last_node=Util.instance_field(bg, Util.settings_field,last_node,
				"user","Username/Wallet", device.linked_miner.pool.username);
			last_node=Util.instance_field(bg, Util.settings_field, last_node,
				"pass","Password/Pool args", device.linked_miner.pool.password);
			last_node=device.linked_miner.pool.setup_nodes(bg, device, last_node);
			last_node=Util.instance_field(bg, Util.settings_button, last_node,
				"advanced","Advanced Settings", device.linked_miner.advanced_settings);
			if(device.linked_miner.advanced_settings):
				last_node= Util.instance_field(bg, Util.settings_field, last_node,
					"worker","Instance Name", device.linked_miner.pool.worker);
				if(device.is_cpu()):
					last_node=Util.instance_field(bg, Util.settings_field, last_node,
						"threads","Threads", str(device.linked_miner.threads));
				
				last_node=device.linked_miner.pool.setup_advanced_nodes(bg, device, last_node);
	
			for node in bg.get_children():
				if(node.get_size().y+node.get_position().y+node.get_margin(MARGIN_BOTTOM)>=bg.rect_min_size.y):
					bg.rect_min_size.y=node.get_size().y+node.get_position().y+node.get_margin(MARGIN_BOTTOM)+5;
	pass;

static func show_settings(node_above, device, scroller):
	#all nodes are relative to the position of the first node, so we only need to offset the first one for scrolling
	if node_above==null:
		device.display_bg.set_begin(Vector2(0,-scroller));
	else:
		device.display_bg.set_begin(Util.vec_relative(Vector2(0,30),node_above));

	device.display_bg.color=Color(1,1,1,0);
	device.display_bg.rect_size=Vector2(800, device.instanced_widget.get_size().y);
	if(!device.display_bg.has_node("widget_bg/Label_device")):
		return device.display_bg;
	device.display_bg.get_node("widget_bg/Label_device").text=device.device_name;
	if(device.linked_miner!=null && device.linked_miner.pool!=null):
		device.linked_miner.pool.username = device.display_bg.get_node("widget_bg/user/TextEdit").text;
		device.linked_miner.pool.password = device.display_bg.get_node("widget_bg/pass/TextEdit").text;
		device.linked_miner.advanced_settings = device.display_bg.get_node("widget_bg/advanced/Button").pressed;
		if(device.display_bg.has_node("widget_bg/worker/TextEdit")):
			if(!device.linked_miner.advanced_settings):
				Util.clear_widget(device.display_bg.get_node("widget_bg"));
				device.display_bg.get_node("widget_bg").replace_by(settings_scene.instance(),false);
				init_settings(null, device);
				return device.display_bg;
			else:
				device.linked_miner.pool.worker = device.display_bg.get_node("widget_bg/worker/TextEdit").text;
		elif(device.linked_miner.advanced_settings):
			Util.clear_widget(device.display_bg.get_node("widget_bg"));
			device.display_bg.get_node("widget_bg").replace_by(settings_scene.instance(),false);
			init_settings(null, device);
			return device.display_bg;
			
		device.linked_miner.pool.update_nodes(device);
	if(device.linked_miner.advanced_settings && device.display_bg.get_node_or_null("widget_bg/threads")!=null):
		device.linked_miner.threads = int(device.display_bg.get_node("widget_bg/threads/TextEdit").text);
	return device.display_bg;
	
func _notification(what):
	if what == MainLoop.NOTIFICATION_CRASH:
		pass # the game crashed
	
static func setup_pool_single_device(host_display, device):
	Util.set_widget(host_display, device, device_scene);
	pass;
	
	
static func display_pool_single_device(node_above, device, scroller):
	#all nodes are relative to the position of the first node, so we only need to offset the first one for scrolling
	if node_above==null:
		device.display_bg.set_begin(Vector2(0,-scroller));
	else:
		device.display_bg.set_begin(Util.vec_relative(Vector2(0,30),node_above));
	
	#init display
	if(device.display_bg.get_node("widget_bg/device_name").text=="Device_name"):
		device.display_bg.color=Color(1,1,1,0);
		device.display_bg.get_node("widget_bg/device_name").text = device.device_name;
		if(device.linked_miner!=null && device.linked_miner.pool!=null):
			device.display_bg.get_node("widget_bg/pool_name").text = device.linked_miner.pool.getDisplayName();
		else:
			device.display_bg.get_node("widget_bg/pool_name").text = "No Pool Selected";
			device.display_bg.get_node_or_null("widget_bg/VBox_pool").queue_free();
			device.display_bg.get_node_or_null("widget_bg/VBox_chain").queue_free();
		device.display_bg.rect_size=Vector2(800, device.instanced_widget.get_size().y);
		
	
	#regular display value updates
	display_value(device.get_clock(),"MHz",get_node_address("clock_core"), device,true,true,0,0);
	
	display_value(device.get_temp(),"C",get_node_address("temp_core"), device,true,true,40,70);
	display_value(device.get_temp_avg(),"C",get_node_address("temp_avg"), device,true,true,40,70);
	display_value(device.get_temp_memory(),"C",get_node_address("temp_memory"), device,true,true,40,70);
	if(HardwareMonitor.is_gpu_amd(device.id)):
		var box = device.display_bg.get_node_or_null("widget_bg/VBox_hw/temp_core");
		if(box!=null):
			box.text="Hottest Sensor";
		box = device.display_bg.get_node_or_null("widget_bg/VBox_hw/temp_memory");
		if(box!=null):
			box.text="Coldest Sensor";
	display_value(device.get_clock_memory(),"MHz",get_node_address("clock_memory"), device,true,true,0,0);
	display_value(device.get_fan(),"RPM",get_node_address("fan"), device,true,true,0,0);
	display_value(device.get_watts(),"W",get_node_address("watt"), device,true,true,0,0);
	
	if(device.linked_miner!=null && device.linked_miner.pool!=null):
		display_value(device.linked_miner.hashrateTotal,"H/s",get_node_address("hashrate"), device,false,true,0,0);
		display_value(device.linked_miner.hashrateAvg,"H/s",get_node_address("hashrate_avg"), device,false,true,0,0);
		display_value(device.linked_miner.accepted,"|"+str(device.linked_miner.rejected),get_node_address("shares"), device,false,true,0,0);
		#todo: shares/hour
		display_value(device.linked_miner.poolPing,"ms",get_node_address("ping"), device,false,true,0,0);
		display_value(device.linked_miner.poolBlock,"",get_node_address("block"), device,false,true,0,0);
		display_value(device.linked_miner.poolDifficulty,"",get_node_address("diff"), device,false,true,0,0);
		display_value(device.linked_miner.poolEpoch,"",get_node_address("epoch"), device,false,true,0,0);
		if(device.linked_miner.uptime>60):
			display_value((device.linked_miner.uptime/60),"h",get_node_address("uptime"), device,false,true,0,0);
		else:
			display_value(device.linked_miner.uptime,"m",get_node_address("uptime"), device,false,true,0,0);
		display_value(device.linked_miner.cooling_time,"m",get_node_address("cool_time"), device,false,true,0,0);
		display_value(device.linked_miner.pool_revenue,"",get_node_address("revenue"), device,false,true,0,0);
	
	return device.display_bg;
	
	
	
static func setup_pool_multi_device(node_above, pool):
	for dev in pool.linked_devices:
		setup_pool_single_device(node_above, dev);
		pass;
	pass;

	
static func display_pool_multi_device(node_above, pool, scroller):
	
	pass;



static func get_node_address(value):
	match(value):
		"clock_core":
			return "widget_bg/VBox_hw/clock_core/";
		"temp_core":
			return "widget_bg/VBox_hw/temp_core/";
		"temp_avg":
			return "widget_bg/VBox_hw/temp_avg/";
		"temp_memory":
			return "widget_bg/VBox_hw/temp_memory/";
		"clock_memory":
			return "widget_bg/VBox_hw/clock_memory/";
		"fan":
			return "widget_bg/VBox_hw/fan_speed/";
		"watt":
			return "widget_bg/VBox_hw/voltage/";
		"hashrate":
			return "widget_bg/VBox_pool/hashrate/";
		"hashrate_avg":
			return "widget_bg/VBox_pool/hashrate_avg/";
		"shares":
			return "widget_bg/VBox_pool/shares/";
		"ping":
			return "widget_bg/VBox_pool/ping/";
		"block":
			return "widget_bg/VBox_chain/block/";
		"diff":
			return "widget_bg/VBox_chain/diff/";
		"epoch":
			return "widget_bg/VBox_chain/epoch/";
		"revenue":
			return "widget_bg/VBox_chain/revenue/";
		"uptime":
			return "widget_bg/VBox_chain/uptime/";
		"cool_time":
			return "widget_bg/VBox_chain/cooling_time/";
			
	return null;


static func display_value(var val:int, var value_suffix, var node, var device, var hide_empty, var value_subnode, var low:int, var high:int):
	#skip function if node doesn't exist
	UserData.current_node=device.display_bg.get_node_or_null(node);
	if(UserData.current_node==null):
		return;
	
	#remove the node if it shouldn't exist, then clear the variable.
	if(hide_empty && val==0):
		UserData.current_node.queue_free();
		UserData.current_node=null;
		return;
	
	if(value_subnode):
		UserData.current_node=UserData.current_node.get_node("value");
	
	UserData.current_node.text=str(val)+value_suffix;
	if(val <low || low==0):
		UserData.current_node.add_color_override("font_color", Color(0,1,0,1));
	elif(val>high):
		UserData.current_node.add_color_override("font_color", Color(1,0,0,1));
	else:
		var col:float=val/high;
		UserData.current_node.add_color_override("font_color", Color(col,1-col,0,1));
