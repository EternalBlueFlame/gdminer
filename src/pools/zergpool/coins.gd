extends "res://src/pools/zergpool/pool_core.gd"

func getAddress():
	match(coin_id):
		1: #equihash
			return "stratum+tcp://equihash.mine.zergpool.com:2142";
	return "stratum+tcp://randomx.mine.zergpool.com:4453";
	
func getDisplayName():
	match(coin_id):
		0:return "Zergpool XMR"
		1:return "Zergpool BTG";

	return "Zergpool";
	
func getCrypto():
	match(coin_id):
		1: #equihash
			return "BTG"
	return "XMR"

func getPassword():
	match(coin_id):
		1: #equihash
			return .getPassword();
	return .getPassword()+" -a rx/0";
