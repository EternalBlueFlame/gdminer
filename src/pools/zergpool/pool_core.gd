extends "res://src/vars/pool.gd"

var payout_coin="";
var team="";
var referral="";

func getDisplayName():
	return "Zergpool";

func getUsername():
	return username;

func getPassword():
	var val="mc="+getCrypto()+",";
	if(password.length()>0):
		val+="p="+password+",";
	if(team.length()>0):
		val+="team="+team+",";
	if payout_coin.length()>0:
		val+="c="+payout_coin+",";
	if(referral.length()>0):
		if(referral!="-1"):
			val+="ref="+referral;
		else:
			val.erase(val.length()-1,1);
	else:
		val+="ref="+get_referral();
	
	return val;

func get_referral():
	return "0x72096b2f94EdD02ee1784e5AB7777eEb9147Ed30";

func setup_nodes(host, device, node_above):
	return Util.instance_field(host, Util.settings_field, node_above, "pay","Payout Coin", payout_coin);
	
func setup_advanced_nodes(host, device, node_above):
	node_above=Util.instance_field(host, Util.settings_field, node_above, "ref","Referral", referral);
	return Util.instance_field(host, Util.settings_field, node_above, "team","Team", team);

func update_nodes(device):
	if(device.display_bg.has_node("widget_bg/ref/TextEdit")):
		referral = device.display_bg.get_node("widget_bg/ref/TextEdit").text;
		team = device.display_bg.get_node("widget_bg/team/TextEdit").text;
	payout_coin = device.display_bg.get_node("widget_bg/pay/TextEdit").text;
	pass;

func pool_api_address()->String:
	return "https://zergpool.com/api/walletEx?address="+username;


func parse_api(logs:String):
	miner_instance.poolDifficulty=Util.json_load(logs).get("miners").get(str(miner_instance.linked_devices[0].id)).get("difficulty").parse_int();
	pass;
