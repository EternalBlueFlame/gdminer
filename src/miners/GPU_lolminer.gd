extends "res://src/vars/miner_core.gd"


func get_run_string(pool:miningPool, port:int):
	var command:String =get_folder();
	
	if OS.get_name().to_lower()=="windows":
		command+="lolminer.exe ";
	else: #all else fails expect it's a linux-like OS, including android
		command+="lolminer";
	
	command += " -p " + pool.getAddress() + " -u " + pool.getUsername();
		
	if (pool.getPassword().length() >0):
		command+= " -pass " + pool.getPassword();
	
	if(pool.getWorker().length()>0):
		 command+= " -worker " + pool.getWorker();
	
	command += " --devices ";
	for dev in linked_devices:
		command+=str(dev.id)+",";
	command=command.substr(0,command.length()-1);#remove the excess comma
	
	command +=" --no-color on" + " --apiport="+str(port);
	return command;

func get_rest_address():
	return "";#there's no sub-address, just port number which is inherant
	
func check_version():
	pass

func get_name():
	return "lolminer 1.76"

func parse_logs(logs:String):
	#TODO:
	#hashrateTotal=float(Util.json_load(logs).get("hashrate").get("total").parse_array()[0]);
	#poolDifficulty=Util.json_load(logs).get("connection").get("diff").parse_float();
	#poolPing=Util.json_load(logs).get("connection").get("ping").parse_float();
	#accepted=Util.json_load(logs).get("connection").get("accepted").parse_float();
	#rejected=Util.json_load(logs).get("connection").get("rejected").parse_float();
	#poolBlock=Util.json_load(logs).get("blocks").get(pool.getCrypto()).get("height").parse_int();
	pass;

func download_versions(scene):
	print_debug("download started");
	var file:File = File.new();
	if (OS.get_name().to_lower()=="windows"):
		if(!OS.file_exists(get_folder()+"lolminer.exe")):
			get_file("https://github.com/Lolliedieb/lolMiner-releases/releases/download/1.76/lolMiner_v1.76_Win64.zip");
	else: #all else fails expect it's a linux-like OS, including android
		#i think the linux build is both ARM and x86? if not we'll need to custom build ARM.
		if(!file.file_exists(get_folder()+"lolminer")):
			get_file("https://github.com/Lolliedieb/lolMiner-releases/releases/download/1.76/lolMiner_v1.76_Lin64.tar.gz");
		else:
			print_debug("exists!")
	file.close();
	pass
	

func get_folder() -> String:
	return Util.mkdirs("/binaries/lolminer/"+get_name().replace(" ","")+"/");

