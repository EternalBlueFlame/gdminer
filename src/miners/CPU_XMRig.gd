extends "res://src/vars/miner_core.gd"

var threads:int=1;


func get_run_string(pool:miningPool, port:int):
	var command:String =get_folder();
	
	if OS.get_name().to_lower()=="windows":
		command+="xmrig.exe ";
	else: #all else fails expect it's a linux-like OS, including android
		command+="xmrig";
	
	command += " -o " + pool.getAddress() + " -u " + pool.getUsername();
		
	if (pool.getPassword().length() >0):
		command+= " -p " + pool.getPassword();
	
	if(pool.getWorker().length()>0):
		 command+= " -w " + pool.getWorker();
		
	command +=" -t "+str(threads) + " --no-color" + " --http-port="+str(port);
	return command;
	
func get_rest_address():
	return "/2/summary";
	
func check_version():
	pass

func get_name():
	return "XMRig-16.8.1"

func parse_logs(logs:String):
	hashrateTotal=float(Util.json_load(logs).get("hashrate").get("total").parse_array()[0]);
	poolDifficulty=Util.json_load(logs).get("connection").get("diff").parse_float();
	poolPing=Util.json_load(logs).get("connection").get("ping").parse_float();
	accepted=Util.json_load(logs).get("connection").get("accepted").parse_float();
	rejected=Util.json_load(logs).get("connection").get("rejected").parse_float();
	poolBlock=Util.json_load(logs).get("blocks").get(pool.getCrypto()).get("height").parse_int();
	pass;

func download_versions(scene):
	print_debug("download started");
	var file:File = File.new();
	if (OS.get_name().to_lower()=="windows"):
		if(!OS.file_exists(get_folder()+"xmrig.exe")):
			get_file("https://github.com/xmrig/xmrig/releases/download/v6.18.1/xmrig-6.18.1-gcc-win64.zip");
	elif OS.get_name().to_lower()=="OSX":
		if OS.has_feature("arm64") || OS.has_feature("arm"):
			if(!OS.file_exists(get_folder()+"xmrig")):
				get_file("https://github.com/xmrig/xmrig/releases/download/v6.18.1/xmrig-6.18.1-macos-arm64.tar.gz");
		else:
			if(!OS.file_exists(get_folder()+"xmrig")):
				get_file("https://github.com/xmrig/xmrig/releases/download/v6.18.1/xmrig-6.18.1-macos-x64.tar.gz");
	else: #all else fails expect it's a linux-like OS, including android
		#i think the linux build is both ARM and x86? if not we'll need to custom build ARM.
		if(!file.file_exists(get_folder()+"xmrig")):
			get_file("https://github.com/xmrig/xmrig/releases/download/v6.18.1/xmrig-6.18.1-linux-x64.tar.gz");
		else:
			print_debug("exists!")
	file.close();
	pass
	

func get_folder() -> String:
	return Util.mkdirs("/binaries/xmrig/"+get_name()+"/");

