# GDMiner

This project is designed to provide a graphical interface for managing 3rd party crypto mining software, alongside providing safeties like overheat prevention and clear-language explanations/fixes for common issues and misconceptions.

Language: GDScript, C# (not yet implemented)

Renderer: GLES 2

For usage limitations please see the LICENSE file