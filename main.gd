extends Node

const Display_render = preload("res://src/common/main_display.gd");

var button_manager = load("res://src/common/button_manager.gd").new();

var devices = [];


var color_bg: Color = Color(0.125,0.125,0.15,1);

var sidebar: VBoxContainer = VBoxContainer.new();
var main_display: VBoxContainer;
var display_scrollbar: VScrollBar;

var settings_button: Button;
var start_button: Button;
var logs_button: Button;
var credits_button: Button;
var support_button: Button;
var menu:int=0;
var scrollmax:int=0;
var port_available=4000;

var running:bool=false;



# Called when the node enters the scene tree for the first time.
func _ready():
	
	main_display=get_node("miner_container/miner_display");
	display_scrollbar=get_node("miner_scroll_bar");
	
	button_manager.Main=self;
	
	button_manager._ready();
	
	var devIndex=0;
	
	for cpu in HardwareMonitor.get_cpu_count():
		devices.append(load("res://src/vars/cpu.gd").new());
		devices[devIndex].set_miner(load("res://src/miners/CPU_XMRig.gd").new());
		devices[devIndex].set_pool(load("res://src/pools/zergpool/coins.gd").new().setCoinID(0));
		devices[devIndex].id=cpu;
		devices[devIndex].device_name=HardwareMonitor.get_cpu_name(cpu);
		devIndex+=1;
	
	for gpu in HardwareMonitor.get_gpu_count(): 
		devices.append(load("res://src/vars/device.gd").new());
		devices[devIndex].set_miner(load("res://src/miners/GPU_lolminer.gd").new());
		devices[devIndex].set_pool(load("res://src/pools/zergpool/coins.gd").new().setCoinID(1));
		devices[devIndex].id=gpu;
	#	print(HardwareMonitor.get_gpu_name(gpu));
		devices[devIndex].device_name=HardwareMonitor.get_gpu_name(gpu);
		devIndex+=1;
	
	init_screen();
	
	
	

	pass;


func _input(event):
	button_manager.scrollbar(event);
	return;



# Called every frame. 'delta' is the elapsed time since the previous frame.
#TODO: add some timers 
func _process(delta):
	
	if(menu!=8):
		var oldm=null;
		var pools =[];
		for dev in devices:
		#switch statements are called "match" COME ON. Use. Standardizedev. Names. Andev. Symbols!
		#if that ends up being my biggest complaint, this is a pretty good language.
		#underscore is the wildcard
			match(menu):
				0:
					if (dev.linked_miner!=null && !pools.has(dev.linked_miner) && 
					dev.linked_miner.linked_devices!=null && dev.linked_miner.linked_devices.size()>2):
						pools.append(dev);
						oldm=Display_render.display_pool_multi_device(oldm, dev.linked_miner, 
						(display_scrollbar.value));
					oldm=Display_render.display_pool_single_device(oldm, dev, (display_scrollbar.value));
					pass;
				1:
					oldm=Display_render.show_settings(oldm, dev, (display_scrollbar.value));
					pass;
				4:
					app_log.set_begin(Vector2(0,-display_scrollbar.value));
					draw_main_logs();
					pass;
			dev.update_tick(self, oldm);
	else:
		draw_main_about();
		pass
		
	
	button_manager._process(delta);
	
	
	#calc scroll value, sub display area size, plus half the margin, then add size for each part and their offsets.
	scrollmax=-get_node("miner_container").get_rect().size.y-15;
	for d in devices:
		if(d.display_bg!=null):
			 #there's always a flat 30 margin, as managed by @main_display.gd
			scrollmax+=(d.display_bg.get_rect().size.y+30);
		pass
	if(menu==4):
		scrollmax=app_log.text.split("\n").size()*100;
	display_scrollbar.set_max(scrollmax);
	
	pass


var app_log = Label.new();
func init_screen():
	button_manager.init_sidebar();
	Util.clear_widget(main_display);
	var pools =[];
	
	match(menu):
		0:
			for dev in devices:
				if (dev.linked_miner!=null && !pools.has(dev.linked_miner)): 
					pools.append(dev.linked_miner);
					if (dev.linked_miner.linked_devices!=null && dev.linked_miner.linked_devices.size()>1):
						Display_render.setup_pool_multi_device(main_display, dev.linked_miner);
						pass;
				Display_render.setup_pool_single_device(main_display, dev);
			pass;
		1:
			for dev in devices:
				Display_render.init_settings(main_display, dev);
			pass;
		4:
			app_log.text = HardwareMonitor.debugLog;
			for dev in devices:
				if (dev.linked_miner!=null && !pools.has(dev.linked_miner)): 
					pools.append(dev.linked_miner);
					app_log.text+="\n\n ------------------\n\n"+ dev.device_name + " : " + dev.linked_miner.pool.getDisplayName()+"\nCommand:\n";
					var pos=0;
					while pos< dev.linked_miner.get_run_string(dev.linked_miner.pool, dev.linked_miner.port).length():
						app_log.text+="\n"+  dev.linked_miner.get_run_string(dev.linked_miner.pool, dev.linked_miner.port).substr(pos,100);
						pos+=100;
					for interval in dev.linked_miner.minerLog:
						for line in interval:
							app_log.text+="\n"+line;
						
						
			main_display.add_child(app_log);
			pass;
		
	pass


func draw_main_settings():
	pass


func draw_main_logs():
	pass


func draw_main_about():
	pass
